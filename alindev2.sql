-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2019 at 02:54 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dating`
--

-- --------------------------------------------------------

--
-- Table structure for table `body_type`
--

CREATE TABLE `body_type` (
  `body_type_id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `body_type`
--

INSERT INTO `body_type` (`body_type_id`, `name`) VALUES
(1, 'athletic'),
(2, 'skinny'),
(3, 'heavy');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `region_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `name`, `region_id`) VALUES
(1, 'Berlin', 3),
(21535, 'Stuttgart', 4),
(21536, 'Frankfurt', 5),
(21537, 'Mannheim', 4),
(21538, 'Hamburg', 6),
(21539, 'Essen', 7),
(21540, 'Duisburg', 7),
(21541, 'Munich', 8),
(21542, 'Düsseldorf', 7),
(21543, 'Cologne', 7),
(21544, 'Wuppertal', 7),
(21545, 'Saarbrücken', 9),
(21546, 'Marienberg', 8),
(21547, 'Bremen', 10),
(21548, 'Hannover', 11),
(21549, 'Bonn', 7),
(21550, 'Dresden', 12),
(21551, 'Wiesbaden', 5),
(21552, 'Dortmund', 7),
(21553, 'Leipzig', 12),
(21554, 'Heidelberg', 4),
(21555, 'Karlsruhe', 4),
(21556, 'Augsburg', 8),
(21557, 'Bielefeld', 7),
(21558, 'Koblenz', 13),
(21559, 'Altchemnitz', 12),
(21560, 'Kassel', 5),
(21561, 'Münster', 7),
(21562, 'Kiel', 14),
(21563, 'Freiburg', 4),
(21564, 'Braunschweig', 11),
(21565, 'Fürth', 8),
(21566, 'Lübeck', 14),
(21567, 'Osnabrück', 11),
(21568, 'Magdeburg', 15),
(21569, 'Potsdam', 16),
(21570, 'Erfurt', 17),
(21571, 'Rostock', 18),
(21572, 'Mainz', 13),
(21573, 'Ulm', 4),
(21574, 'Würzburg', 8),
(21575, 'Oldenburg', 11),
(21576, 'Regensburg', 8),
(21577, 'Ingolstadt', 8),
(21578, 'Göttingen', 11),
(21579, 'Bremerhaven', 10),
(21580, 'Cottbus', 16),
(21581, 'Jena', 17),
(21582, 'Gera', 17),
(21583, 'Flensburg', 14),
(21584, 'Schwerin', 18),
(21585, 'Rosenheim', 8),
(21586, 'Gießen', 5),
(21587, 'Stralsund', 18),
(21588, 'Coburg', 8),
(21589, 'Hofeck', 8),
(21590, 'Emden', 11),
(21591, 'Detmold', 7),
(21592, 'Meißen', 12),
(21593, 'Kitzingen', 8),
(21594, 'Dingolfing', 8),
(21595, 'Heppenheim', 5),
(21596, 'Torgau', 12),
(21597, 'Hanau', 5),
(21598, 'Husum', 14),
(21599, 'Schwandorf', 8),
(21600, 'Bitburg', 13),
(21601, 'Cham', 8),
(21602, 'Traunstein', 8),
(21603, 'Lüchow', 11),
(21604, 'Gifhorn', 11),
(21605, 'Biberach', 4),
(21606, 'Bad Reichenhall', 8),
(21607, 'Künzelsau', 4),
(21608, 'Weißenburg', 8),
(21609, 'Regen', 8),
(21610, 'Nuremberg', 8),
(21611, 'Aurich', 11),
(21612, 'Nordhorn', 11),
(21613, 'Aichach', 8),
(21614, 'Marburg', 5),
(21615, 'Görlitz', 12),
(21616, 'Vechta', 11),
(21617, 'Trier', 13),
(21618, 'Pirmasens', 13),
(21619, 'Pirna', 12),
(21620, 'Neustadt', 13),
(21621, 'Beeskow', 16),
(21622, 'Westerstede', 11),
(21623, 'Verden', 11),
(21624, 'Worms', 13),
(21625, 'Düren', 7),
(21626, 'Landsberg', 8),
(21627, 'Ludwigsburg', 4),
(21628, 'Meiningen', 17),
(21629, 'Siegen', 7),
(21630, 'Deggendorf', 8),
(21631, 'Peine', 11),
(21632, 'Frankfurt (Oder)', 16),
(21633, 'Nienburg', 11),
(21634, 'Brake', 11),
(21635, 'Memmingen', 8),
(21636, 'Kirchheimbolanden', 13),
(21637, 'Tauberbischofsheim', 4),
(21638, 'Emmendingen', 4),
(21639, 'Warendorf', 7),
(21640, 'Bad Segeberg', 14),
(21641, 'Rotenburg', 11),
(21642, 'Kronach', 8),
(21643, 'Darmstadt', 5),
(21644, 'Mindelheim', 8),
(21645, 'Bergheim', 7),
(21646, 'Donauwörth', 8),
(21647, 'Korbach', 5),
(21648, 'Herzberg', 16),
(21649, 'Hameln', 11),
(21650, 'Suhl', 17),
(21651, 'Friedberg', 5),
(21652, 'Hof', 8),
(21653, 'Neuburg', 8),
(21654, 'Bad Kissingen', 8),
(21655, 'Viersen', 7),
(21656, 'Birkenfeld', 13),
(21657, 'Bad Fallingbostel', 11),
(21658, 'Halle', 15),
(21659, 'Bamberg', 8),
(21660, 'Fürstenfeldbruck', 8),
(21661, 'Neuss', 7),
(21662, 'Bad Kreuznach', 13),
(21663, 'Heilbronn', 4),
(21664, 'Bad Ems', 13),
(21665, 'Schwäbisch Hall', 4),
(21666, 'Offenburg', 4),
(21667, 'Saalfeld', 17),
(21668, 'Wolfenbüttel', 11),
(21669, 'Altenkirchen', 13),
(21670, 'Pforzheim', 4),
(21671, 'Günzburg', 8),
(21672, 'Euskirchen', 7),
(21673, 'Chemnitz', 12),
(21674, 'Rendsburg', 14),
(21675, 'Tirschenreuth', 8),
(21676, 'Offenbach', 5),
(21677, 'Uelzen', 11),
(21678, 'Zwickau', 12),
(21679, 'Schwabach', 8),
(21680, 'Gelsenkirchen', 7),
(21681, 'Mettmann', 7),
(21682, 'Ravensburg', 4),
(21683, 'Leer', 11),
(21684, 'Wittmund', 11),
(21685, 'Ingelheim', 13),
(21686, 'Höxter', 7),
(21687, 'Oranienburg', 16),
(21688, 'Erbach', 5),
(21689, 'Freising', 8),
(21690, 'Landau', 13),
(21691, 'Stendal', 15),
(21692, 'Balingen', 4),
(21693, 'Reutlingen', 4),
(21694, 'Eisenach', 17),
(21695, 'Tuttlingen', 4),
(21696, 'Neumünster', 14),
(21697, 'Neu-Ulm', 8),
(21698, 'Köthen', 15),
(21699, 'Schleiz', 17),
(21700, 'Garmisch-Partenkirchen', 8),
(21701, 'Baden-Baden', 4),
(21702, 'Bayreuth', 8),
(21703, 'Wunsiedel', 8),
(21704, 'Osterode', 11),
(21705, 'Sankt Wendel', 9),
(21706, 'Lüdenscheid', 7),
(21707, 'Plauen', 12),
(21708, 'Forst', 16),
(21709, 'Pfaffenhofen', 8),
(21710, 'Bochum', 7),
(21711, 'Weimar', 17),
(21712, 'Wilhelmshaven', 11),
(21713, 'Limburg', 5),
(21714, 'Freyung', 8),
(21715, 'Merseburg', 15),
(21716, 'Halberstadt', 15),
(21717, 'Dessau-Roßlau', 15),
(21718, 'Weiden', 8),
(21719, 'Altenburg', 17),
(21720, 'Heide', 14),
(21721, 'Böblingen', 4),
(21722, 'Kulmbach', 8),
(21723, 'Homberg', 5),
(21724, 'Perleberg', 16),
(21725, 'Mülheim', 7),
(21726, 'Northeim', 11),
(21727, 'Salzwedel', 15),
(21728, 'Cuxhaven', 11),
(21729, 'Plön', 14),
(21730, 'Mühlhausen', 17),
(21731, 'Kempten', 8),
(21732, 'Güstrow', 18),
(21733, 'Lichtenfels', 8),
(21734, 'Bad Salzungen', 17),
(21735, 'Weilheim', 8),
(21736, 'Jever', 11),
(21737, 'Arnstadt', 17),
(21738, 'Lüneburg', 11),
(21739, 'Delmenhorst', 11),
(21740, 'Neubrandenburg', 18),
(21741, 'Bad Dürkheim', 13),
(21742, 'Greiz', 17),
(21743, 'Altötting', 8),
(21744, 'Erding', 8),
(21745, 'Lübben', 16),
(21746, 'Holzminden', 11),
(21747, 'Wetzlar', 5),
(21748, 'Soest', 7),
(21749, 'Mosbach', 4),
(21750, 'Heilbad Heiligenstadt', 17),
(21751, 'Neustadt', 8),
(21752, 'Calw', 4),
(21753, 'Kleve', 7),
(21754, 'Annaberg-Buchholz', 12),
(21755, 'Wismar', 18),
(21756, 'Aachen', 7),
(21757, 'Tübingen', 4),
(21758, 'Freiberg', 12),
(21759, 'Mönchengladbach', 7),
(21760, 'Nordhausen', 17),
(21761, 'Krefeld', 7),
(21762, 'Stadthagen', 11),
(21763, 'Hildesheim', 11),
(21764, 'Celle', 11),
(21765, 'Eberswalde', 16),
(21766, 'Recklinghausen', 7),
(21767, 'Eisenberg', 17),
(21768, 'Kaufbeuren', 8),
(21769, 'Sömmerda', 17),
(21770, 'Remscheid', 7),
(21771, 'Greifswald', 18),
(21772, 'Rastatt', 4),
(21773, 'Naumburg', 15),
(21774, 'Lauf', 8),
(21775, 'Amberg', 8),
(21776, 'Ratzeburg', 14),
(21777, 'Bad Homburg', 5),
(21778, 'Herne', 7),
(21779, 'Sangerhausen', 15),
(21780, 'Forchheim', 8),
(21781, 'Eutin', 14),
(21782, 'Bad Oldesloe', 14),
(21783, 'Kelheim', 8),
(21784, 'Bad Neustadt', 8),
(21785, 'Helmstedt', 11),
(21786, 'Heinsberg', 7),
(21787, 'Zweibrücken', 13),
(21788, 'Hagen', 7),
(21789, 'Montabaur', 13),
(21790, 'Haßfurt', 8),
(21791, 'Pinneberg', 14),
(21792, 'Apolda', 17),
(21793, 'Bad Schwalbach', 5),
(21794, 'Marktoberdorf', 8),
(21795, 'Winsen', 11),
(21796, 'Wesel', 7),
(21797, 'Landshut', 8),
(21798, 'Alzey', 13),
(21799, 'Homburg', 9),
(21800, 'Passau', 8),
(21801, 'Cloppenburg', 11),
(21802, 'Miesbach', 8),
(21803, 'Gotha', 17),
(21804, 'Schwelm', 7),
(21805, 'Kusel', 13),
(21806, 'Meschede', 7),
(21807, 'Steinfurt', 7),
(21808, 'Aschaffenburg', 8),
(21809, 'Paderborn', 7),
(21810, 'Karlstadt', 8),
(21811, 'Waiblingen', 4),
(21812, 'Villingen-Schwenningen', 4),
(21813, 'Rottweil', 4),
(21814, 'Göppingen', 4),
(21815, 'Eichstätt', 8),
(21816, 'Freudenstadt', 4),
(21817, 'Schleswig', 14),
(21818, 'Erlangen', 8),
(21819, 'Olpe', 7),
(21820, 'Lörrach', 4),
(21821, 'Ansbach', 8),
(21822, 'Wittlich', 13),
(21823, 'Neuruppin', 16),
(21824, 'Sonneberg', 17),
(21825, 'Bottrop', 7),
(21826, 'Ludwigshafen', 13),
(21827, 'Borken', 7),
(21828, 'Starnberg', 8),
(21829, 'Gummersbach', 7),
(21830, 'Lauterbach', 5),
(21831, 'Herford', 7),
(21832, 'Rathenow', 16),
(21833, 'Solingen', 7),
(21834, 'Speyer', 13),
(21835, 'Siegburg', 7),
(21836, 'Burg', 15),
(21837, 'Leverkusen', 7),
(21838, 'Unna', 7),
(21839, 'Coesfeld', 7),
(21840, 'Cochem', 13),
(21841, 'Eschwege', 5),
(21842, 'Bad Hersfeld', 5),
(21843, 'Bad Neuenahr-Ahrweiler', 13),
(21844, 'Sondershausen', 17),
(21845, 'Dachau', 8),
(21846, 'Meppen', 11),
(21847, 'Wolfsburg', 11),
(21848, 'Brandenburg', 16),
(21849, 'Sigmaringen', 4),
(21850, 'Sonthofen', 8),
(21851, 'Itzehoe', 14),
(21852, 'Bergisch Gladbach', 7),
(21853, 'Dillingen', 8),
(21854, 'Saarlouis', 9),
(21855, 'Groß-Gerau', 5),
(21856, 'Oberhausen', 7),
(21857, 'Goslar', 11),
(21858, 'Neustadt', 5),
(21859, 'Germersheim', 13),
(21860, 'Hofheim', 5),
(21861, 'Ebersberg', 8),
(21862, 'Prenzlau', 16),
(21863, 'Bad Tölz', 8),
(21864, 'Parchim', 18),
(21865, 'Luckenwalde', 16),
(21866, 'Bernburg', 15),
(21867, 'Osterholz-Scharmbeck', 11),
(21868, 'Stade', 11),
(21869, 'Neumarkt', 8),
(21870, 'Salzgitter', 11),
(21871, 'Bautzen', 12),
(21872, 'Hildburghausen', 17),
(21873, 'Heidenheim', 4),
(21874, 'Wittenberg', 15),
(21875, 'Kaiserslautern', 13),
(21876, 'Miltenberg', 8),
(21877, 'Fulda', 5),
(21878, 'Senftenberg', 16),
(21879, 'Mühldorf', 8),
(21880, 'Merzig', 9),
(21881, 'Seelow', 16),
(21882, 'Minden', 7),
(21883, 'Waldshut-Tiengen', 4),
(21884, 'Neunkirchen', 9),
(21885, 'Neuwied', 13),
(21886, 'Daun', 13),
(21887, 'Esslingen', 4),
(21888, 'Simmern', 13),
(21889, 'Gütersloh', 7),
(21890, 'Diepholz', 11),
(21891, 'Frankenthal', 13),
(21892, 'Straubing', 8),
(21893, 'Pfarrkirchen', 8),
(21894, 'Hamm', 7),
(21895, 'Haldensleben', 15),
(21896, 'Aalen', 4),
(22045, 'Paris', 34),
(22046, 'Lyon', 35),
(22047, 'Marseille', 36),
(22048, 'Lille', 37),
(22049, 'Nice', 36),
(22050, 'Toulouse', 38),
(22051, 'Bordeaux', 39),
(22052, 'Rouen', 40),
(22053, 'Strasbourg', 41),
(22054, 'Nantes', 42),
(22055, 'Metz', 41),
(22056, 'Grenoble', 35),
(22057, 'Toulon', 36),
(22058, 'Montpellier', 38),
(22059, 'Nancy', 41),
(22060, 'Saint-Étienne', 35),
(22061, 'Melun', 34),
(22062, 'Le Havre', 40),
(22063, 'Tours', 43),
(22064, 'Clermont-Ferrand', 35),
(22065, 'Orléans', 43),
(22066, 'Mulhouse', 41),
(22067, 'Rennes', 44),
(22068, 'Reims', 41),
(22069, 'Caen', 40),
(22070, 'Angers', 42),
(22071, 'Dijon', 45),
(22072, 'Nîmes', 38),
(22073, 'Limoges', 39),
(22074, 'Aix-en-Provence', 36),
(22075, 'Perpignan', 38),
(22076, 'Biarritz', 39),
(22077, 'Brest', 44),
(22078, 'Le Mans', 42),
(22079, 'Amiens', 37),
(22080, 'Besançon', 45),
(22081, 'Annecy', 35),
(22082, 'Calais', 37),
(22083, 'Poitiers', 39),
(22084, 'Versailles', 34),
(22085, 'Kerbrient', 44),
(22086, 'Béziers', 38),
(22087, 'La Rochelle', 39),
(22088, 'Roanne', 35),
(22089, 'Bourges', 43),
(22090, 'Arras', 37),
(22091, 'Troyes', 41),
(22092, 'Cherbourg', 40),
(22093, 'Agen', 39),
(22094, 'Tarbes', 38),
(22095, 'Ajaccio', 46),
(22096, 'Saint-Brieuc', 44),
(22097, 'Nevers', 45),
(22098, 'Vichy', 35),
(22099, 'Dieppe', 40),
(22100, 'Auxerre', 45),
(22101, 'Bastia', 46),
(22102, 'Châlons-en-Champagne', 41),
(22108, 'Rome', 49),
(22109, 'Rome', 80),
(22110, 'Milan', 50),
(22111, 'Milan', 81),
(22112, 'Naples', 51),
(22113, 'Naples', 82),
(22114, 'Turin', 52),
(22115, 'Turin', 83),
(22116, 'Florence', 53),
(22117, 'Florence', 84),
(22118, 'Salerno', 51),
(22119, 'Salerno', 82),
(22120, 'Palermo', 54),
(22121, 'Palermo', 85),
(22122, 'Catania', 54),
(22123, 'Catania', 85),
(22124, 'Genoa', 55),
(22125, 'Genoa', 86),
(22126, 'Bari', 56),
(22127, 'Bari', 87),
(22128, 'Bologna', 57),
(22129, 'Bologna', 88),
(22130, 'Verona', 58),
(22131, 'Verona', 89),
(22132, 'Pescara', 59),
(22133, 'Pescara', 90),
(22134, 'Cagliari', 60),
(22135, 'Cagliari', 91),
(22136, 'Venice', 58),
(22137, 'Venice', 89),
(22138, 'Messina', 54),
(22139, 'Messina', 85),
(22140, 'Como', 50),
(22141, 'Como', 81),
(22142, 'Caserta', 51),
(22143, 'Caserta', 82),
(22144, 'Trieste', 61),
(22145, 'Trieste', 92),
(22146, 'Pisa', 53),
(22147, 'Pisa', 84),
(22148, 'Taranto', 56),
(22149, 'Taranto', 87),
(22150, 'Bergamo', 50),
(22151, 'Bergamo', 81),
(22152, 'Reggio di Calabria', 62),
(22153, 'Reggio di Calabria', 93),
(22154, 'Treviso', 58),
(22155, 'Treviso', 89),
(22156, 'Modena', 57),
(22157, 'Modena', 88),
(22158, 'Parma', 57),
(22159, 'Parma', 88),
(22160, 'Lecce', 56),
(22161, 'Lecce', 87),
(22162, 'Livorno', 53),
(22163, 'Livorno', 84),
(22164, 'Foggia', 56),
(22165, 'Foggia', 87),
(22166, 'Perugia', 63),
(22167, 'Perugia', 94),
(22168, 'Ravenna', 57),
(22169, 'Ravenna', 88),
(22170, 'Ferrara', 57),
(22171, 'Ferrara', 88),
(22172, 'Siracusa', 54),
(22173, 'Siracusa', 85),
(22174, 'Sassari', 60),
(22175, 'Sassari', 91),
(22176, 'Udine', 61),
(22177, 'Udine', 92),
(22178, 'Barletta', 56),
(22179, 'Barletta', 87),
(22180, 'Trento', 64),
(22181, 'Trento', 95),
(22182, 'Brindisi', 56),
(22183, 'Brindisi', 87),
(22184, 'Novara', 52),
(22185, 'Novara', 83),
(22186, 'Ancona', 65),
(22187, 'Ancona', 96),
(22188, 'Soprabolzano', 64),
(22189, 'Soprabolzano', 95),
(22190, 'Catanzaro', 62),
(22191, 'Catanzaro', 93),
(22192, 'Arezzo', 53),
(22193, 'Arezzo', 84),
(22194, 'Marsala', 54),
(22195, 'Marsala', 85),
(22196, 'Asti', 52),
(22197, 'Asti', 83),
(22198, 'Potenza', 66),
(22199, 'Potenza', 97),
(22200, 'Ragusa', 54),
(22201, 'Ragusa', 85),
(22202, 'L’Aquila', 59),
(22203, 'L’Aquila', 90),
(22204, 'Benevento', 51),
(22205, 'Benevento', 82),
(22206, 'Civitavecchia', 49),
(22207, 'Civitavecchia', 80),
(22208, 'Crotone', 62),
(22209, 'Crotone', 93),
(22210, 'Siena', 53),
(22211, 'Siena', 84),
(22212, 'Campobasso', 67),
(22213, 'Campobasso', 98),
(22214, 'Olbia', 60),
(22215, 'Olbia', 91),
(22216, 'Aosta', 68),
(22217, 'Aosta', 99),
(22218, 'Vibo Valentia', 62),
(22219, 'Vibo Valentia', 93),
(22220, 'Padova', 58),
(22221, 'Padova', 89),
(22222, 'Savona', 55),
(22223, 'Savona', 86),
(22224, 'Caltanissetta', 54),
(22225, 'Caltanissetta', 85),
(22226, 'Vicenza', 58),
(22227, 'Vicenza', 89),
(22228, 'Gorizia', 61),
(22229, 'Gorizia', 92),
(22230, 'Rieti', 49),
(22231, 'Rieti', 80),
(22232, 'Grosseto', 53),
(22233, 'Grosseto', 84),
(22234, 'Bolzano', 64),
(22235, 'Bolzano', 95),
(22236, 'Massa', 53),
(22237, 'Massa', 84),
(22238, 'Sanluri', 60),
(22239, 'Sanluri', 91),
(22240, 'Latina', 49),
(22241, 'Latina', 80),
(22242, 'Vercelli', 52),
(22243, 'Vercelli', 83),
(22244, 'Belluno', 58),
(22245, 'Belluno', 89),
(22246, 'Cremona', 50),
(22247, 'Cremona', 81),
(22248, 'Oristano', 60),
(22249, 'Oristano', 91),
(22250, 'Mantova', 50),
(22251, 'Mantova', 81),
(22252, 'Prato', 53),
(22253, 'Prato', 84),
(22254, 'Enna', 54),
(22255, 'Enna', 85),
(22256, 'Lucca', 53),
(22257, 'Lucca', 84),
(22258, 'Viterbo', 49),
(22259, 'Viterbo', 80),
(22260, 'Villacidro', 60),
(22261, 'Villacidro', 91),
(22262, 'Trani', 56),
(22263, 'Trani', 87),
(22264, 'Pavia', 50),
(22265, 'Pavia', 81),
(22266, 'Piacenza', 57),
(22267, 'Piacenza', 88),
(22268, 'Monza', 50),
(22269, 'Monza', 81),
(22270, 'Verbania', 52),
(22271, 'Verbania', 83),
(22272, 'Rimini', 57),
(22273, 'Rimini', 88),
(22274, 'Andria', 56),
(22275, 'Andria', 87),
(22276, 'Fermo', 65),
(22277, 'Fermo', 96),
(22278, 'Nuoro', 60),
(22279, 'Nuoro', 91),
(22280, 'Alessandria', 52),
(22281, 'Alessandria', 83),
(22282, 'Matera', 66),
(22283, 'Matera', 97),
(22284, 'Pistoia', 53),
(22285, 'Pistoia', 84),
(22286, 'Reggio Emilia', 57),
(22287, 'Reggio Emilia', 88),
(22288, 'Frosinone', 49),
(22289, 'Frosinone', 80),
(22290, 'Imperia', 55),
(22291, 'Imperia', 86),
(22292, 'Tortolì', 60),
(22293, 'Tortolì', 91),
(22294, 'Iglesias', 60),
(22295, 'Iglesias', 91),
(22296, 'Tempio Pausania', 60),
(22297, 'Tempio Pausania', 91),
(22298, 'Trapani', 54),
(22299, 'Trapani', 85),
(22300, 'Rovigo', 58),
(22301, 'Rovigo', 89),
(22302, 'Teramo', 59),
(22303, 'Teramo', 90),
(22304, 'Sondrio', 50),
(22305, 'Sondrio', 81),
(22306, 'Lanusei', 60),
(22307, 'Lanusei', 91),
(22308, 'Biella', 52),
(22309, 'Biella', 83),
(22310, 'Cosenza', 62),
(22311, 'Cosenza', 93),
(22312, 'Cuneo', 52),
(22313, 'Cuneo', 83),
(22314, 'Ascoli Piceno', 65),
(22315, 'Ascoli Piceno', 96),
(22316, 'Avellino', 51),
(22317, 'Avellino', 82),
(22318, 'Chieti', 59),
(22319, 'Chieti', 90),
(22320, 'Terni', 63),
(22321, 'Terni', 94),
(22322, 'Varese', 50),
(22323, 'Varese', 81),
(22324, 'Forlì', 57),
(22325, 'Forlì', 88),
(22326, 'Lecco', 50),
(22327, 'Lecco', 81),
(22328, 'Carbonia', 60),
(22329, 'Carbonia', 91),
(22330, 'Macerata', 65),
(22331, 'Macerata', 96),
(22332, 'La Spezia', 55),
(22333, 'La Spezia', 86),
(22334, 'Pesaro', 65),
(22335, 'Pesaro', 96),
(22336, 'Pordenone', 61),
(22337, 'Pordenone', 92),
(22338, 'Lodi', 50),
(22339, 'Lodi', 81),
(22340, 'Brescia', 50),
(22341, 'Brescia', 81),
(22342, 'Agrigento', 54),
(22343, 'Agrigento', 85);

-- --------------------------------------------------------

--
-- Table structure for table `config_settings`
--

CREATE TABLE `config_settings` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` int(11) NOT NULL,
  `info` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `name`) VALUES
(1, 'Germany'),
(2, 'France'),
(3, 'Italy');

-- --------------------------------------------------------

--
-- Table structure for table `credits`
--

CREATE TABLE `credits` (
  `credit_id` int(11) NOT NULL,
  `credit_name` varchar(45) NOT NULL,
  `credit_number` varchar(100) NOT NULL,
  `credit_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credits`
--

INSERT INTO `credits` (`credit_id`, `credit_name`, `credit_number`, `credit_price`) VALUES
(1, 'standard', '200', 20),
(2, 'premium', '2000', 20);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`) VALUES
(1, 'male'),
(2, 'female');

-- --------------------------------------------------------

--
-- Table structure for table `image_uploads`
--

CREATE TABLE `image_uploads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(512) NOT NULL,
  `thumbnail_small_path` varchar(512) NOT NULL,
  `thumbnail_large_path` varchar(512) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_uploads`
--

INSERT INTO `image_uploads` (`id`, `user_id`, `path`, `thumbnail_small_path`, `thumbnail_large_path`, `created_at`, `updated_at`) VALUES
(32, 25, 'uploads/users/44631.png', '', '', '2019-06-03 10:19:11', '2019-06-03 10:19:11'),
(33, 24, 'uploads/users/34741.jpeg', '', '', '2019-06-03 10:21:06', '2019-06-03 10:21:06'),
(35, 26, 'uploads/users/353251.png', '', '', '2019-06-03 10:22:10', '2019-06-03 10:22:10'),
(36, 24, 'uploads/users/435305.png', '', '', '2019-06-03 10:26:42', '2019-06-03 10:26:42'),
(39, 24, 'uploads/users/132784.jpeg', '', '', '2019-06-03 10:43:29', '2019-06-03 10:43:29'),
(40, 27, 'uploads/users/260851.jpeg', '', '', '2019-06-03 10:45:30', '2019-06-03 10:45:30'),
(44, 28, 'uploads/users/98063.png', '', '', '2019-06-03 13:03:38', '2019-06-03 13:03:38'),
(45, 25, 'uploads/users/295183.jpg', '', '', '2019-06-04 08:36:14', '2019-06-04 08:36:14'),
(47, 29, 'uploads/users/486975.png', '', '', '2019-06-04 09:11:11', '2019-06-04 09:11:11'),
(49, 28, 'uploads/users/87126.jpeg', '', '', '2019-06-04 13:26:34', '2019-06-04 13:26:34'),
(50, 29, 'uploads/users/421440.jpeg', '', '', '2019-06-05 10:31:01', '2019-06-05 10:31:01'),
(51, 25, 'uploads/users/324650.jpg', '', '', '2019-06-05 15:53:49', '2019-06-05 15:53:49');

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

CREATE TABLE `interests` (
  `interest_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`interest_id`, `name`) VALUES
(1, 'Biking'),
(2, 'Camping'),
(3, 'Diving'),
(4, 'Sports'),
(5, 'Movies'),
(6, 'Fashion'),
(7, 'Fashion & Hunting'),
(8, 'Games'),
(9, 'Hobbies & Crafts'),
(10, 'Hockey'),
(11, 'Lying on the beach'),
(12, 'Meditations & Yoga'),
(13, 'Museums & Art'),
(14, 'Music & Concerts'),
(15, 'Nature'),
(16, 'Party & Night Clubs'),
(17, 'Reading books'),
(18, 'Sailling'),
(19, 'Shopping'),
(20, 'Travelling'),
(21, 'Watching TV');

-- --------------------------------------------------------

--
-- Table structure for table `look_for`
--

CREATE TABLE `look_for` (
  `id` int(12) NOT NULL,
  `hash_id` varchar(12) DEFAULT NULL,
  `user_id` int(12) DEFAULT NULL,
  `gender_id` int(12) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `look_for`
--

INSERT INTO `look_for` (`id`, `hash_id`, `user_id`, `gender_id`, `age`, `description`) VALUES
(1, NULL, 24, 2, 29, 'To be very happy'),
(2, NULL, 25, 2, 29, 'To know to cook');

-- --------------------------------------------------------

--
-- Table structure for table `marital_status`
--

CREATE TABLE `marital_status` (
  `marital_status_id` int(11) NOT NULL,
  `marital_status_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marital_status`
--

INSERT INTO `marital_status` (`marital_status_id`, `marital_status_name`) VALUES
(1, 'maried'),
(2, 'in relationship');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`) VALUES
(1, 'AdminUser'),
(2, 'RecruitedUsers'),
(3, 'PaymentController'),
(4, 'credit'),
(6, 'messages'),
(7, 'configuration'),
(8, 'Packages'),
(9, 'proceses'),
(10, 'AdminGuest'),
(11, 'login'),
(12, 'AdminGallery'),
(13, 'AdminHome'),
(14, 'UserApi'),
(15, 'LocationApi');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(12) NOT NULL,
  `hash_id` varchar(12) DEFAULT NULL,
  `message` text,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `send_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `read_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(12) NOT NULL,
  `hash_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `duration` smallint(6) NOT NULL,
  `description` text NOT NULL,
  `video_chat` smallint(6) NOT NULL,
  `live_chat` smallint(6) NOT NULL,
  `view_likes` smallint(6) NOT NULL,
  `send_winks` smallint(6) NOT NULL,
  `view_visitors` smallint(6) NOT NULL,
  `max_photos` smallint(6) NOT NULL,
  `max_messages` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `hash_id`, `name`, `price`, `duration`, `description`, `video_chat`, `live_chat`, `view_likes`, `send_winks`, `view_visitors`, `max_photos`, `max_messages`) VALUES
(1, '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', 'bronz', '10', 43, 'description', 0, 1, 1, 0, 0, 213, 423),
(2, 'd4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35', 'gold', '15', 32, 'gold description', 0, 0, 0, 0, 0, 12, 120),
(3, '4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce', 'platinium', '200', 200, 'description platinium', 1, 1, 1, 1, 0, 32, 200),
(4, 'ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d', 'test', '32', 32, 'description test', 0, 0, 0, 0, 0, 32, 3232),
(5, 'ef2d127de37b942baad06145e54b0c619a1f22327b2ebbcfbec78f5564afe39d', 'test1', '321', 34, 'desc', 0, 0, 0, 0, 0, 432, 432);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(12) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `moderator` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `payments_code` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `admined_by` int(11) DEFAULT NULL,
  `moderated_by` int(11) DEFAULT NULL,
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `meniu_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `role_id`, `meniu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 2, 3),
(5, 1, 3),
(6, 1, 4),
(7, 1, 10),
(9, 1, 11),
(10, 2, 11),
(11, 2, 10),
(12, 3, 10),
(13, 3, 11),
(14, 1, 12),
(15, 1, 13),
(16, 3, 1),
(17, 3, 2),
(18, 1, 8),
(19, 2, 1),
(20, 1, 14),
(21, 1, 15),
(22, 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hash_id` varchar(255) NOT NULL,
  `sign_id` int(11) NOT NULL,
  `credit_id` int(11) NOT NULL,
  `mood_id` int(11) NOT NULL,
  `marital_status_id` int(11) NOT NULL,
  `study_id` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `source_link` varchar(255) NOT NULL,
  `body_type_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `is_online` int(11) DEFAULT NULL,
  `is_robot` int(11) DEFAULT NULL,
  `admin_by` int(11) DEFAULT NULL,
  `moderated_by` int(11) DEFAULT NULL,
  `description` text,
  `activated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `hash_id`, `sign_id`, `credit_id`, `mood_id`, `marital_status_id`, `study_id`, `age`, `height`, `source_link`, `body_type_id`, `package_id`, `is_online`, `is_robot`, `admin_by`, `moderated_by`, `description`, `activated_at`, `created_at`, `updated_at`) VALUES
(1, 24, '', 2, 1, 1, 1, 3, 32, 157, 'www.google.com', 1, 1, 1, 1, 24, 24, '', '2019-05-30 12:57:45', '2019-05-30 12:57:45', '2019-05-30 12:57:45'),
(2, 25, '', 2, 1, 1, 1, 1, 32, 146, 'www.google.com', 3, 1, 1, 1, 5, 24, '', '2019-05-30 13:30:15', '2019-05-30 13:30:15', '2019-05-30 13:30:15'),
(3, 26, '', 2, 1, 1, 1, 0, 32, 197, 'www.google.com', 1, 1, 1, 1, 5, 26, '', '2019-05-31 08:34:31', '2019-05-31 08:34:31', '2019-05-31 08:34:31'),
(4, 27, '', 2, 1, 1, 1, 0, 32, 154, 'www.google.com', 2, 3, 1, 1, 25, 27, '', '2019-06-03 10:45:30', '2019-06-03 10:45:30', '2019-06-03 10:45:30'),
(5, 28, '', 2, 1, 1, 1, 0, 32, 155, 'www.google.com', 2, 2, 1, 1, 28, 26, '', '2019-06-03 10:50:11', '2019-06-03 10:50:11', '2019-06-03 10:50:11'),
(6, 29, '', 2, 1, 1, 1, 0, 32, 154, 'www.google.com', 2, 3, 1, 1, 24, 26, '', '2019-06-04 09:11:11', '2019-06-04 09:11:11', '2019-06-04 09:11:11');

-- --------------------------------------------------------

--
-- Table structure for table `profile_photo`
--

CREATE TABLE `profile_photo` (
  `profile_id` int(11) NOT NULL,
  `profile_photo` varchar(255) DEFAULT NULL,
  `cover_photo` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recruited_members`
--

CREATE TABLE `recruited_members` (
  `id` int(11) NOT NULL,
  `hash_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(45) NOT NULL,
  `age` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `moderated_by` int(11) NOT NULL,
  `country` varchar(100) NOT NULL,
  `region` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `from_source` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruited_members`
--

INSERT INTO `recruited_members` (`id`, `hash_id`, `user_id`, `user_name`, `email`, `age`, `added_by`, `moderated_by`, `country`, `region`, `city`, `from_source`, `comment`, `created_at`, `updated_at`) VALUES
(1, '', 0, 'test1', 'test1@gmail.com', 32, 29, 28, 'Romania', 'Moldova', 'Iasi', 'www.ceva.ro', 'comments', '2019-05-30 13:02:52', '2019-05-30 13:02:52'),
(2, '', 0, 'test2', 'test2@gmail.com', 32, 24, 28, 'Romania', 'Moldova', 'Iasi', 'www.ceva.ro', 'comments', '2019-05-31 08:36:14', '2019-05-31 08:36:14'),
(3, '', 0, 'test1', 'test1@gmail.com', 32, 26, 0, 'Italy', 'ffqw', 'fq', 'www.ceva.ro', 'fsad', '2019-07-04 09:29:08', '2019-07-04 09:29:08');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `region_id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`region_id`, `name`, `country_id`) VALUES
(3, 'Berlin', 1),
(4, 'Baden-Württemberg', 1),
(5, 'Hesse', 1),
(6, 'Hamburg', 1),
(7, 'North Rhine-Westphalia', 1),
(8, 'Bavaria', 1),
(9, 'Saarland', 1),
(10, 'Bremen', 1),
(11, 'Lower Saxony', 1),
(12, 'Saxony', 1),
(13, 'Rhineland-Palatinate', 1),
(14, 'Schleswig-Holstein', 1),
(15, 'Saxony-Anhalt', 1),
(16, 'Brandenburg', 1),
(17, 'Thuringia', 1),
(18, 'Mecklenburg-Western Pomerania', 1),
(34, 'Île-de-France', 2),
(35, 'Auvergne-Rhône-Alpes', 2),
(36, 'Provence-Alpes-Côte d’Azur', 2),
(37, 'Hauts-de-France', 2),
(38, 'Occitanie', 2),
(39, 'Nouvelle-Aquitaine', 2),
(40, 'Normandie', 2),
(41, 'Grand Est', 2),
(42, 'Pays de la Loire', 2),
(43, 'Centre-Val de Loire', 2),
(44, 'Bretagne', 2),
(45, 'Bourgogne-Franche-Comté', 2),
(46, 'Corsica', 2),
(49, 'Lazio', 3),
(50, 'Lombardy', 3),
(51, 'Campania', 3),
(52, 'Piedmont', 3),
(53, 'Tuscany', 3),
(54, 'Sicilia', 3),
(55, 'Liguria', 3),
(56, 'Puglia', 3),
(57, 'Emilia-Romagna', 3),
(58, 'Veneto', 3),
(59, 'Abruzzo', 3),
(60, 'Sardegna', 3),
(61, 'Friuli-Venezia Giulia', 3),
(62, 'Calabria', 3),
(63, 'Umbria', 3),
(64, 'Trentino-Alto Adige', 3),
(65, 'Marche', 3),
(66, 'Basilicata', 3),
(67, 'Molise', 3),
(68, 'Valle d’Aosta', 3),
(80, 'Lazio', 3),
(81, 'Lombardy', 3),
(82, 'Campania', 3),
(83, 'Piedmont', 3),
(84, 'Tuscany', 3),
(85, 'Sicilia', 3),
(86, 'Liguria', 3),
(87, 'Puglia', 3),
(88, 'Emilia-Romagna', 3),
(89, 'Veneto', 3),
(90, 'Abruzzo', 3),
(91, 'Sardegna', 3),
(92, 'Friuli-Venezia Giulia', 3),
(93, 'Calabria', 3),
(94, 'Umbria', 3),
(95, 'Trentino-Alto Adige', 3),
(96, 'Marche', 3),
(97, 'Basilicata', 3),
(98, 'Molise', 3),
(99, 'Valle d’Aosta', 3);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'subadmin'),
(3, 'moderator'),
(4, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `signs`
--

CREATE TABLE `signs` (
  `sign_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signs`
--

INSERT INTO `signs` (`sign_id`, `name`) VALUES
(1, 'varsator'),
(2, 'leu');

-- --------------------------------------------------------

--
-- Table structure for table `status_account`
--

CREATE TABLE `status_account` (
  `status_account_id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_account`
--

INSERT INTO `status_account` (`status_account_id`, `name`) VALUES
(1, 'active'),
(2, 'disabled');

-- --------------------------------------------------------

--
-- Table structure for table `studies`
--

CREATE TABLE `studies` (
  `id` int(12) NOT NULL,
  `study_name` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studies`
--

INSERT INTO `studies` (`id`, `study_name`) VALUES
(1, 'high school'),
(2, 'college'),
(3, 'associates degre'),
(4, 'graduate degree'),
(5, 'phd');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `hash_id` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `status_account_id` int(11) DEFAULT NULL,
  `gdpr_accepted` tinyint(4) DEFAULT NULL,
  `activated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `hash_id`, `email`, `user_name`, `password`, `gender_id`, `status_account_id`, `gdpr_accepted`, `activated_at`, `created_at`, `updated_at`) VALUES
(5, '0.40613597483014313', 'dumitru.mihailalin@gmail.com', 'alin', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'a295fa4e457ca8c72751ffb6196f34b2349dcd91443b8c70ad76082d30dbdcd9', 'test1@gmail.com', 'test1', 'b444ac06613fc8d63795be9ad0beaf55011936ac', 1, 1, 1, '2019-05-30 12:57:45', '2019-05-30 12:57:45', '2019-05-30 12:57:45'),
(25, 'cc2e166955ec49675e749f9dce21db0cbd2979d4aac4a845bdde35ccb642bc47', 'test2@gmail.com', 'test2', '109f4b3c50d7b0df729d299bc6f8e9ef9066971f', 1, 1, 1, '2019-05-30 13:30:15', '2019-05-30 13:30:15', '2019-05-30 13:30:15'),
(26, '5659dab8f4feabee273722ed625ff7b8436728c51d7718f7e2714f5fdc8ca63e', 'test3@gmail.com', 'test3', '3ebfa301dc59196f18593c45e519287a23297589', 1, 1, 1, '2019-05-31 08:34:31', '2019-05-31 08:34:31', '2019-05-31 08:34:31'),
(27, '9c9c16e4d14854bf57669f7c2863cc00cefacdeba71005b26d503f1c2c49e9b7', 'test4@gmail.com', 'test4', '1ff2b3704aede04eecb51e50ca698efd50a1379b', 1, 1, 1, '2019-06-03 10:45:30', '2019-06-03 10:45:30', '2019-06-03 10:45:30'),
(28, 'd26978d51aaf68cfd98c0ce181911333544212df9205a60e1ec09c0ad352f6d9', 'test5@gmail.com', 'test5', '911ddc3b8f9a13b5499b6bc4638a2b4f3f68bf23', 1, 1, 1, '2019-06-03 10:50:11', '2019-06-03 10:50:11', '2019-06-03 10:50:11'),
(29, '9011273a31d66571b83b26741945bf68e478c7eca64eb83a4812fab1eb50a52c', 'test6@gmail.com', 'test6', 'a66df261120b6c2311c6ef0b1bab4e583afcbcc0', 1, 1, 1, '2019-06-04 09:11:11', '2019-06-04 09:11:11', '2019-06-04 09:11:11');

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_table`
-- (See below for the actual view)
--
CREATE TABLE `users_table` (
`moderated_by` varchar(100)
,`added_by` varchar(100)
,`id` int(11)
,`user_name` varchar(100)
,`email` varchar(100)
,`created_at` timestamp
,`GDPR` tinyint(4)
,`status_account` int(11)
,`role` varchar(255)
,`age` int(11)
,`is_online` int(11)
,`is_robot` int(11)
,`description` text
,`height` int(11)
,`package` varchar(255)
,`body_type` varchar(255)
,`sign` varchar(45)
,`marital` varchar(50)
,`country` varchar(50)
,`region` varchar(60)
,`city` varchar(60)
);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `user_id`, `country_id`, `region_id`, `city_id`) VALUES
(19, 24, 2, 34, 22078),
(20, 25, 2, 40, 22062),
(21, 26, 1, 9, 21884),
(22, 27, 3, 60, 22292),
(23, 28, 3, 62, 22310),
(24, 29, 2, 42, 22078);

-- --------------------------------------------------------

--
-- Table structure for table `user_choice`
--

CREATE TABLE `user_choice` (
  `id` int(11) NOT NULL,
  `user_id` int(12) DEFAULT NULL,
  `choice_id` int(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_choice`
--

INSERT INTO `user_choice` (`id`, `user_id`, `choice_id`) VALUES
(1, 6, 1),
(444, 28, 16),
(443, 28, 14),
(398, 26, 16),
(397, 26, 14),
(396, 26, 10),
(395, 26, 8),
(379, 29, 14),
(378, 29, 10),
(377, 29, 8),
(376, 29, 4),
(442, 28, 10),
(375, 29, 1),
(441, 28, 9),
(440, 28, 7),
(439, 28, 3),
(438, 28, 1),
(425, 27, 21),
(424, 27, 9),
(423, 27, 7),
(422, 27, 1),
(408, 25, 14),
(407, 25, 10),
(394, 26, 4),
(334, 24, 16),
(97, 5, 16),
(96, 5, 8),
(95, 5, 4),
(406, 25, 8),
(405, 25, 4),
(333, 24, 5),
(404, 25, 1),
(332, 24, 3),
(331, 24, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_hobbies`
--

CREATE TABLE `user_hobbies` (
  `id` int(11) NOT NULL,
  `user_id` int(12) DEFAULT NULL,
  `smoke_id` int(12) DEFAULT NULL,
  `drink_id` int(12) DEFAULT NULL,
  `tatoo_id` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_hobbies`
--

INSERT INTO `user_hobbies` (`id`, `user_id`, `smoke_id`, `drink_id`, `tatoo_id`) VALUES
(2, 24, 2, 2, 2),
(3, 25, 2, 2, 2),
(4, 26, 2, 2, 2),
(5, 27, 1, 1, 2),
(6, 28, 2, 1, 1),
(7, 29, 2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_mood`
--

CREATE TABLE `user_mood` (
  `id` int(12) NOT NULL,
  `mood` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_mood`
--

INSERT INTO `user_mood` (`id`, `mood`) VALUES
(1, 'happy'),
(2, 'excited'),
(3, 'energized');

-- --------------------------------------------------------

--
-- Table structure for table `user_preferences`
--

CREATE TABLE `user_preferences` (
  `preference_id` int(11) NOT NULL,
  `preference_name` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_preferences`
--

INSERT INTO `user_preferences` (`preference_id`, `preference_name`) VALUES
(1, 'yes'),
(2, 'no');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(5, 1),
(7, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 1),
(26, 3),
(27, 1),
(28, 3),
(29, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wink`
--

CREATE TABLE `wink` (
  `id` int(11) NOT NULL,
  `sender` int(11) DEFAULT NULL,
  `receiver` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `users_table`
--
DROP TABLE IF EXISTS `users_table`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_table`  AS  select distinct `moderatoruser`.`user_name` AS `moderated_by`,`adminusers`.`user_name` AS `added_by`,`activeuser`.`id` AS `id`,`activeuser`.`user_name` AS `user_name`,`activeuser`.`email` AS `email`,`activeuser`.`created_at` AS `created_at`,`activeuser`.`gdpr_accepted` AS `GDPR`,`activeuser`.`status_account_id` AS `status_account`,`roles`.`name` AS `role`,`profiles`.`age` AS `age`,`profiles`.`is_online` AS `is_online`,`profiles`.`is_robot` AS `is_robot`,`profiles`.`description` AS `description`,`profiles`.`height` AS `height`,`packages`.`name` AS `package`,`body_type`.`name` AS `body_type`,`signs`.`name` AS `sign`,`marital_status`.`marital_status_name` AS `marital`,`countries`.`name` AS `country`,`regions`.`name` AS `region`,`cities`.`name` AS `city` from (((((((((((((`users` `activeuser` left join `profiles` on((`profiles`.`user_id` = `activeuser`.`id`))) left join `packages` on((`packages`.`id` = `profiles`.`package_id`))) left join `body_type` on((`body_type`.`body_type_id` = `profiles`.`body_type_id`))) left join `user_roles` on((`user_roles`.`user_id` = `activeuser`.`id`))) left join `roles` on((`roles`.`id` = `user_roles`.`role_id`))) left join `user_address` on((`user_address`.`user_id` = `activeuser`.`id`))) left join `countries` on((`countries`.`country_id` = `user_address`.`country_id`))) left join `regions` on((`regions`.`region_id` = `user_address`.`region_id`))) left join `cities` on((`cities`.`city_id` = `user_address`.`city_id`))) left join `users` `moderatoruser` on((`profiles`.`moderated_by` = `moderatoruser`.`id`))) left join `users` `adminusers` on((`profiles`.`admin_by` = `adminusers`.`id`))) left join `signs` on((`signs`.`sign_id` = `profiles`.`sign_id`))) left join `marital_status` on((`marital_status`.`marital_status_id` = `profiles`.`marital_status_id`))) group by `activeuser`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `body_type`
--
ALTER TABLE `body_type`
  ADD PRIMARY KEY (`body_type_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`),
  ADD KEY `cities_FI_1` (`region_id`);

--
-- Indexes for table `config_settings`
--
ALTER TABLE `config_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `credits`
--
ALTER TABLE `credits`
  ADD PRIMARY KEY (`credit_id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_uploads`
--
ALTER TABLE `image_uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_uploads_FI_1` (`user_id`);

--
-- Indexes for table `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`interest_id`);

--
-- Indexes for table `look_for`
--
ALTER TABLE `look_for`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marital_status`
--
ALTER TABLE `marital_status`
  ADD PRIMARY KEY (`marital_status_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_FI_1` (`user_id`),
  ADD KEY `payments_FI_2` (`package_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_FI_1` (`user_id`),
  ADD KEY `profiles_FI_2` (`credit_id`),
  ADD KEY `profiles_FI_3` (`admin_by`),
  ADD KEY `profiles_FI_4` (`moderated_by`),
  ADD KEY `profiles_FI_5` (`mood_id`),
  ADD KEY `profiles_FI_7` (`body_type_id`),
  ADD KEY `profiles_FI_8` (`marital_status_id`),
  ADD KEY `profiles_FI_9` (`sign_id`),
  ADD KEY `profiles_FI_10` (`package_id`);

--
-- Indexes for table `profile_photo`
--
ALTER TABLE `profile_photo`
  ADD PRIMARY KEY (`profile_id`);

--
-- Indexes for table `recruited_members`
--
ALTER TABLE `recruited_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`region_id`),
  ADD KEY `regions_FI_1` (`country_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signs`
--
ALTER TABLE `signs`
  ADD PRIMARY KEY (`sign_id`);

--
-- Indexes for table `status_account`
--
ALTER TABLE `status_account`
  ADD PRIMARY KEY (`status_account_id`);

--
-- Indexes for table `studies`
--
ALTER TABLE `studies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_FI_1` (`gender_id`),
  ADD KEY `users_FI_2` (`status_account_id`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_address_FI_1` (`user_id`);

--
-- Indexes for table `user_choice`
--
ALTER TABLE `user_choice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_hobbies`
--
ALTER TABLE `user_hobbies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_hobbies_FI_1` (`user_id`);

--
-- Indexes for table `user_mood`
--
ALTER TABLE `user_mood`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_preferences`
--
ALTER TABLE `user_preferences`
  ADD PRIMARY KEY (`preference_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_FI_2` (`role_id`);

--
-- Indexes for table `wink`
--
ALTER TABLE `wink`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `body_type`
--
ALTER TABLE `body_type`
  MODIFY `body_type_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22344;

--
-- AUTO_INCREMENT for table `config_settings`
--
ALTER TABLE `config_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `credits`
--
ALTER TABLE `credits`
  MODIFY `credit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `image_uploads`
--
ALTER TABLE `image_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `interest_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `look_for`
--
ALTER TABLE `look_for`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `marital_status`
--
ALTER TABLE `marital_status`
  MODIFY `marital_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `profile_photo`
--
ALTER TABLE `profile_photo`
  MODIFY `profile_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recruited_members`
--
ALTER TABLE `recruited_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `region_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `signs`
--
ALTER TABLE `signs`
  MODIFY `sign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `status_account`
--
ALTER TABLE `status_account`
  MODIFY `status_account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `studies`
--
ALTER TABLE `studies`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user_choice`
--
ALTER TABLE `user_choice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=445;

--
-- AUTO_INCREMENT for table `user_hobbies`
--
ALTER TABLE `user_hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_mood`
--
ALTER TABLE `user_mood`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_preferences`
--
ALTER TABLE `user_preferences`
  MODIFY `preference_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wink`
--
ALTER TABLE `wink`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_FK_1` FOREIGN KEY (`region_id`) REFERENCES `regions` (`region_id`);

--
-- Constraints for table `image_uploads`
--
ALTER TABLE `image_uploads`
  ADD CONSTRAINT `image_uploads_FK_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_FK_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `payments_FK_2` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_FK_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `profiles_FK_10` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`),
  ADD CONSTRAINT `profiles_FK_2` FOREIGN KEY (`credit_id`) REFERENCES `credits` (`credit_id`),
  ADD CONSTRAINT `profiles_FK_3` FOREIGN KEY (`admin_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `profiles_FK_4` FOREIGN KEY (`moderated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `profiles_FK_5` FOREIGN KEY (`mood_id`) REFERENCES `user_mood` (`id`),
  ADD CONSTRAINT `profiles_FK_7` FOREIGN KEY (`body_type_id`) REFERENCES `body_type` (`body_type_id`),
  ADD CONSTRAINT `profiles_FK_8` FOREIGN KEY (`marital_status_id`) REFERENCES `marital_status` (`marital_status_id`),
  ADD CONSTRAINT `profiles_FK_9` FOREIGN KEY (`sign_id`) REFERENCES `signs` (`sign_id`);

--
-- Constraints for table `regions`
--
ALTER TABLE `regions`
  ADD CONSTRAINT `regions_FK_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_FK_1` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`);

--
-- Constraints for table `user_address`
--
ALTER TABLE `user_address`
  ADD CONSTRAINT `user_address_FK_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_hobbies`
--
ALTER TABLE `user_hobbies`
  ADD CONSTRAINT `user_hobbies_FK_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
